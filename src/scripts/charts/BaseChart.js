import Chart from "../charts";

export default class BaseChart {
  constructor({ 
      height = 240, 
      title = "", 
      type = "", 
      data, parent,color = [] 
    }) {
    this.chart_arg = arguments[0];
    this.parent = typeof parent === 'string' ? document.querySelector(parent) : parent;    
    this.title = title;
    this.dom = typeof dom === "string" ? document.querySelector(dom) : parent;
    this.data = data;
    this.chart_types = ['line','pie','bar','scatter','percentage','heatmap'];

    this.set_margins(height);
    this.setup()
  }
  get_dif_chart(type){
    if (!this.chart_types.includes(type)) {
        console.error(`'${type}' is not a valid chart type`)
    }

    if (type === this.type) {
        return;
    }

    //兼容图表类型列表
    let compatible_types = {
        bar: ['line', 'scatter', 'percentage', 'pie'],
        line: ['scatter', 'bar', 'percentage', 'pie'],
        pie: ['line', 'scatter', 'percentage', 'bar'],
        scatter: ['line', 'bar', 'percentage', 'pie'],
        percentage: ['bar', 'line', 'scatter', 'pie'],
        heatmap: []
    }

    if (!compatible_types[this.type].includes(type)) {
        console.error(`'${this.type}' chart cannot be converted to a '${type}' chart.`)
    }

    return new Chart({
        parent: this.chart_arg.parent,
        title: this.title,
        data: this.chart_arg.data,
        type: type,
        height:this.chart_arg.height
    });
  }

  set_margins(height) {
      this.base_height = height;
      this.height = height - 40;
      this.translate_x = 60;
      this.translate_y = 10;
  }

  setup(){
    if(!this.parent){
        console.error("No parent element to render on was provided.");
        return;
    }
    this.refresh(true)
  }

  refresh(init=false){
    this.setup_container()
  }

  setup_container() {
      this.container = 
      `<div>
        <h2 class="title">${this.title}</h2>
        <div class="chart_wrapper"></div>
        <div class="stats_wrapper"></div>
      </div>`

      this.parent.innerHTML = this.container;

      this.chart_wrapper = this.parent.querySelector('.chart_wrapper')
      this.stats_wrapper = this.parent.querySelector('.stats_wrapper')

      this.setup_chart_area()
  }

  setup_chart_area() {
    this.svg = document.createElementNS("http://www.w3.org/2000/svg",'svg');
    this.svg.setAttribute('height',this.height);
    this.svg.setAttribute('width',400);
    this.parent.append(this.svg)
  }
}
